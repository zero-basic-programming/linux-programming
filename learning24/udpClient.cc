#include <iostream>
#include <memory>
#include "udpClient.hpp"
using namespace myclient;
// ./udpClient ip port
static void Usage(string str)
{
    cerr<<"\nUsage\n\t"<< str <<"serverip serverport"<<endl;
}


int main(int argc,char* argv[])
{
    if(argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    string serverip = argv[1];
    uint16_t serverport = atoi(argv[2]);
    unique_ptr<udpClient> ucli(new udpClient(serverip,serverport));
    ucli->initClient();
    ucli->run();
    return 0;
}