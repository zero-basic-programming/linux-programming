#pragma once
#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <functional>
using namespace std;
namespace myserver
{
    static const int max = 1024;
    static const string defaultip = "0.0.0.0"; 
    //回调函数包装器
    using func_t = function<void(int,string,uint16_t,string)>;
    class udpServer
    {
    public:
        udpServer(func_t callback ,const uint16_t &port, const string &ip = defaultip)
            : _port(port), _socketfd(-1), _ip(ip),_callback(callback)
        {
        }
        void initServer()
        {
            // 创建套接字
            _socketfd = socket(AF_INET, SOCK_DGRAM, 0);
            if (_socketfd == -1)
            {
                cerr << "socket fail :" << errno << strerror(errno) << endl;
                exit(1);
            }
            cout<<"socket success :"<<_socketfd<<endl;
            // 网络通信本质就是进程间通信
            // bind关联
            struct sockaddr_in local;
            bzero(&local, sizeof local);
            local.sin_family = AF_INET;
            local.sin_port = htons(_port);
            local.sin_addr.s_addr = inet_addr(_ip.c_str());
            int n = bind(_socketfd, (struct sockaddr *)&local, sizeof(local));
            if (n == -1)
            {
                cerr << "bind fail :" << errno << strerror(errno) << endl;
                exit(2);
            }
        }
        void start()
        {
            // 服务器这里就是一个死循环
            char buffer[max];
            for (;;)
            {
                struct sockaddr_in peer;
                socklen_t len = sizeof(peer);
                ssize_t s = recvfrom(_socketfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&peer, &len);
                if (s > 0)
                {
                    // 接受成功
                    buffer[s] = 0; // 这里保证最后\0
                    string clientip = inet_ntoa(peer.sin_addr);
                    uint16_t clientport = ntohs(peer.sin_port);
                    string message = buffer;
                    // 打印出客户端发过来的信息
                    cout << clientip << "[" << clientport << "] : " << message << endl;
                    //这里开始回调上层传入的回调函数，从而与上层逻辑解耦
                    _callback(_socketfd,clientip,clientport,message);
                }
            }
        }
        ~udpServer()
        {
        }

    private:
        uint16_t _port; // 服务器端口号
        string _ip;     // 一般不建议直接暴露ip，一般不使用
        int _socketfd;  // 套接字
        func_t _callback; //回调函数
    };

}