#include <iostream>
#include <functional>

void print(const char* str,int num)
{
    std::cout<<str<<" "<<num<<std::endl;
}


int main()
{
    print("nihao",10);
    auto func = std::bind(print,"nihao",std::placeholders::_1);
    func(24);
    return 0;
}