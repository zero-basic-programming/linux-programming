#include <stdio.h>
#include <string.h>
#include <mysql/mysql.h>

//定义连接服务器所需要的宏
#define HOST "127.0.0.1"
#define USER "root"
#define PASSWD NULL
#define DBNAME "gobang"
#define PORT 3306


int main()
{
    //1.初始化mysql操作句柄
    MYSQL* mysql = mysql_init(NULL);
    if(mysql == NULL)
    {
        printf("mysql init fail\n");
        mysql_close(mysql);
        return -1;
    }
    //2.连接服务器
    if(mysql_real_connect(mysql,HOST,USER,PASSWD,DBNAME,PORT,NULL,0) == NULL)
    {
        printf("mysql connect fail\n");
        mysql_close(mysql);
        return -1;
    }
    //3.设置字符集
    if(mysql_set_character_set(mysql,"utf8") != 0)
    {
        printf("set character fail\n");
        mysql_close(mysql);
        return -1;
    }   
    //4.连接数据库，在第二步已经完成
    //5.进行数据库的增删改操作
    const char* sql = "insert into stu values(1,18,'曹操')";
    //const char* sql = "update stu set age=20 where id = 1";
    //const char* sql = "delete from stu where id = 1";
    //const char* sql = "select * from stu";
    //const char* sql = "select * from user";
    int ret = mysql_query(mysql,sql);
    if(ret != 0)
    {
        printf("%s ",sql);
        printf("mysql query fail: %s",mysql_errno(mysql));
        mysql_close(mysql);
        return -1;
    }
    // //6.如果有读，那么就先保存查询结果到本地
    // MYSQL_RES* res = mysql_store_result(mysql);
    // if(res == NULL)
    // {
    //     printf("mysql store result fail\n");
    //     mysql_close(mysql);
    //     return -1;
    // }
    // //7.获取结果的行列数
    // int row = mysql_num_rows(res);
    // int field = mysql_num_fields(res);
    // //8.遍历结果集，得到结果
    // for(int i = 0;i<row;++i)
    // {
    //     //获取列
    //     MYSQL_ROW rows= mysql_fetch_row(res);
    //     for(int j = 0;j<field;++j)
    //     {
    //         printf("%s\t",rows[j]);
    //     }
    //     printf("\n");
    // }
    // //9.释放结果集
    // mysql_free_result(res);
    // //10.释放mysql操作句柄
    // mysql_close(mysql);
    return 0;
}

