#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <jsoncpp/json/json.h>

std::string serialize()
{
    // 使用json::value对象存储数据
    Json::Value root;
    root["姓名"] = "小明";
    root["年龄"] = 20;
    root["成绩"].append(90);
    root["成绩"].append(80);
    root["成绩"].append(70);
    // 构建一个StreamWriterBuilder对象
    Json::StreamWriterBuilder swb;
    // 通过StreamWriterBuilder来构建一个额StreamWriter对象
    Json::StreamWriter *sw = swb.newStreamWriter();
    // 使用json进行序列化
    std::stringstream ss;
    int ret = sw->write(root, &ss);
    if (ret != 0)
    {
        std::cerr << "json serilize failed!" << std::endl;
        return "";
    }
    std::cout << "序列化结果: " << ss.str() << std::endl;
    delete sw;
    return ss.str();
}

//反序列化
void unserialize(std::string& s)
{
    //构建一个CharReaderBuilder对象
    Json::CharReaderBuilder crb;
    //通过CharReaderBuilder构建出一个CharReader对象
    Json::CharReader* cb = crb.newCharReader();
    //构建json::value对象来接受反序列化结果
    Json::Value root;
    std::string err;
    //反序列化
    bool ret = cb->parse(s.c_str(),s.c_str()+s.size(),&root,&err);
    if(ret == false)
    {
        std::cerr<<"unserialize failed!"<<std::endl;
        return;
    }
    //输出结果 
    std::cout<<"姓名"<<root["姓名"].asString()<<std::endl;
    std::cout<<"年龄"<<root["年龄"].asInt()<<std::endl;
    int sz = root["成绩"].size();
    for(int i = 0;i<sz;++i)
    {
        std::cout<<"成绩"<<root["成绩"][i]<<std::endl;
    }
    delete cb;
}



int main()
{
    std::string s = serialize();
    unserialize(s);
    return 0;
}
