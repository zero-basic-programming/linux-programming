#pragma once
#include "util.hpp"
#include <unordered_map>
#include <mutex>

class online_user
{
private:
    std::unordered_map<uint64_t, wsserver_t::connection_ptr> _hall_user;
    std::unordered_map<uint64_t, wsserver_t::connection_ptr> _room_user;
    std::mutex _mutex;

public:
    // 添加用户到游戏大厅或者游戏房间的管理
    void enter_game_hall(uint64_t id, wsserver_t::connection_ptr conn)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _hall_user.insert(make_pair(id, conn));
    }
    void enter_game_room(uint64_t id, wsserver_t::connection_ptr conn)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _room_user.insert(make_pair(id, conn));
    }
    // 用户退出游戏大厅/游戏房间
    void exit_game_hall(uint64_t id)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _hall_user.erase(id);
    }
    void exit_game_room(uint64_t id)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _room_user.erase(id);
    }
    // 通过用户id来查看用户是否在游戏大厅/游戏房间
    bool is_in_game_hall(uint64_t id)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _hall_user.find(id);
        if(it == _hall_user.end())
        {
            return false;
        }
        return true;
    }
    bool is_in_game_room(uint64_t id)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _room_user.find(id);
        if(it == _room_user.end())
        {
            return false;
        }
        return true;
    }
    // 通过用户id返回连接
    wsserver_t::connection_ptr get_conn_from_hall(uint64_t id)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _hall_user.find(id);
        if(it == _hall_user.end())
        {
            return wsserver_t::connection_ptr();
        }
        return it->second;
    }
    wsserver_t::connection_ptr get_conn_from_room(uint64_t id)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _room_user.find(id);
        if(it == _room_user.end())
        {
            return wsserver_t::connection_ptr();
        }
        return it->second;
    }
};