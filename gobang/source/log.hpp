#pragma once
#include <cstdio>
#include <ctime>

#define INF 0
#define DEG 1
#define ERR 2
#define DEFAULT_LEVEL INF

#define LOG(level,format,...) do{\
    if(DEFAULT_LEVEL > level) break;\
    time_t t = time(nullptr); \
    struct tm* st = localtime(&t);\
    char buf[32] = {0};\
    strftime(buf,31,"%H-%M-%S",st);\
    fprintf(stdout,"[%s-%s-%d]" format "\n",buf,__FILE__,__LINE__,##__VA_ARGS__);\
}while(0)

#define ILOG(format,...)  LOG(INF,format,##__VA_ARGS__);
#define DLOG(format,...)  LOG(DEG,format,##__VA_ARGS__);
#define ELOG(format,...)  LOG(ERR,format,##__VA_ARGS__);