
#include "server.hpp"

#define HOST "127.0.0.1"
#define USER "root"
#define PASSWD "123456"
#define DBNAME "gobang"
#define PORT 3306

void test_mysql()
{
    MYSQL *mysql = mysql_util::mysql_create(HOST, USER, PASSWD, DBNAME, PORT);
    const char *sql = "insert into stu values(null,18,'李世民')";
    mysql_util::mysql_exec(mysql, sql);
    mysql_util::mysql_destroy(mysql);
}

void test_json()
{
    Json::Value root;
    root["姓名"] = "小明";
    root["年龄"] = 20;
    root["成绩"].append(90);
    root["成绩"].append(80);
    root["成绩"].append(70);
    std::string str;
    //序列化
    json_util::serialize(root,str);
    DLOG("%s",str.c_str());
    //反序列化
    Json::Value val;
    json_util::unserialize(str,val);
    //输出结果 
    std::cout<<"姓名"<<val["姓名"].asString()<<std::endl;
    std::cout<<"年龄"<<val["年龄"].asInt()<<std::endl;
    int sz = val["成绩"].size();
    for(int i = 0;i<sz;++i)
    {
        std::cout<<"成绩"<<val["成绩"][i]<<std::endl;
    }
}

void test_string()
{
    std::string str = ",,...123,456,,,789,,,";
    std::string sep = ",";
    std::vector<std::string> res;
    string_util::spilt(str,sep,res);
    for(auto& str:res)
    {
        DLOG("%s",str.c_str());
    }
}

void test_file()
{
    std::string filename = "./makefile";
    std::string body;
    file_util::read(filename,body);
    std::cout<<body<<std::endl;
}

void test_db()
{
    user_table ut(HOST, USER, PASSWD, DBNAME, PORT);
    Json::Value root;
    root["username"]="xiaoming";
    // root["password"]="123456";
    // // ut.insert(root);
    bool ret = ut.login(root);
    if(ret == false) DLOG("LOGIN FAIL!");

    // bool ret = ut.select_by_id(1,root);
    // if(ret == false)
    // { 
    //     DLOG("NOT FIND");
    //     return;
    // }
    // std::string body;
    // json_util::serialize(root,body);
    // std::cout<<body<<std::endl;
    //ut.lose(1);
}

void test_online()
{
    online_user ou;
    wsserver_t::connection_ptr conn;
    // ou.enter_game_hall(1,conn);
    // if(ou.is_in_game_hall(1))
    // {
    //     DLOG("IN GAME HALL");
    // }
    // else DLOG("NOT IN GAME HALL");
    // ou.exit_game_hall(1);
    // if(ou.is_in_game_hall(1))
    // {
    //     DLOG("IN GAME HALL");
    // }
    // else DLOG("NOT IN GAME HALL");

    ou.enter_game_room(1,conn);
    if(ou.is_in_game_room(1))
    {
        DLOG("IN GAME HALL");
    }
    else DLOG("NOT IN GAME HALL");
    ou.exit_game_room(1);
    if(ou.is_in_game_room(1))
    {
        DLOG("IN GAME HALL");
    }
    else DLOG("NOT IN GAME HALL");
}

int main()
{
    gobang_server _svr(HOST, USER, PASSWD, DBNAME, PORT);
    _svr.start();
    return 0;
}