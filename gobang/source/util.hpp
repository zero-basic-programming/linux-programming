#pragma once
#include "log.hpp"
#include <mysql/mysql.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <memory>
#include <unordered_map>
#include <jsoncpp/json/json.h>
#include <websocketpp/server.hpp>
#include <websocketpp/config/asio_no_tls.hpp>

using wsserver_t = websocketpp::server<websocketpp::config::asio>;

class mysql_util
{
public:
    static MYSQL *mysql_create(const std::string &host, const std::string &username, const std::string &password,
                               const std::string &dbname, uint16_t port)
    {
        // 1.初始化mysql操作句柄
        MYSQL *mysql = mysql_init(nullptr);
        if (mysql == nullptr)
        {
            ELOG("mysql init fail");
            mysql_close(mysql);
            return nullptr;
        }
        // 2.连接服务器
        if (mysql_real_connect(mysql, host.c_str(), username.c_str(), password.c_str(), dbname.c_str(), port, nullptr, 0) == nullptr)
        {
            ELOG("mysql connect fail");
            mysql_close(mysql);
            return nullptr;
        }
        // 3.设置字符集
        if (mysql_set_character_set(mysql, "utf8") != 0)
        {
            ELOG("set character fail");
            mysql_close(mysql);
            return nullptr;
        }
        return mysql;
    }
    static bool mysql_exec(MYSQL *mysql, const std::string &sql)
    {
        int ret = mysql_query(mysql, sql.c_str());
        if (ret != 0)
        {
            ELOG("%s", sql.c_str());
            ELOG("mysql query fail: %s", mysql_errno(mysql));
            return false;
        }
        return true;
    }
    static void mysql_destroy(MYSQL *mysql)
    {
        if (mysql)
        {
            mysql_close(mysql);
        }
    }
};

class json_util
{
public:
    static bool serialize(const Json::Value &root, std::string &str)
    {
        Json::StreamWriterBuilder swb;
        std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter());
        // 使用json进行序列化
        std::stringstream ss;
        int ret = sw->write(root, &ss);
        if (ret != 0)
        {
            ELOG("json serilize failed!");
            return false;
        }
        str = ss.str();
        return true;
    }
    static bool unserialize(const std::string &s, Json::Value &root)
    {
        Json::CharReaderBuilder crb;
        std::unique_ptr<Json::CharReader>cb (crb.newCharReader());
        std::string err;
        // 反序列化
        bool ret = cb->parse(s.c_str(), s.c_str() + s.size(), &root, &err);
        if (ret == false)
        {
            ELOG("unserialize failed!");
            return false;
        }
        return true;
    }
};


class string_util
{
public:
    static int spilt(const std::string& str,const std::string& sep,std::vector<std::string>& res)
    {
        //字符串分割
        //123,456,,,,789  逗号分割 
        int pos,idx = 0;
        while(idx < str.size())
        {
            pos = str.find(sep,idx);
            if(pos == std::string::npos)
            {
                //找到最后一个
                res.push_back(str.substr(idx,pos-idx));
                break;
            }
            //处理重复出现的分割字串
            if(pos == idx)
            {
                idx += sep.size();
                continue;
            }
            res.push_back(str.substr(idx,pos-idx));
            idx = pos + sep.size();
        }
        return res.size();
    }
};


class file_util
{
public:
    static bool read(const std::string& filename,std::string& body)
    {
        //读取文件到body中
        std::ifstream ifs(filename,std::ios::binary);
        if(ifs.is_open() == false)
        {
            ELOG("%s open failed!",filename.c_str());
            return false;
        }
        //获取文件大小
        int fsize = 0;
        ifs.seekg(0,std::ios::end);
        fsize = ifs.tellg();
        ifs.seekg(0,std::ios::beg);
        body.resize(fsize);
        //读取文件
        ifs.read(&body[0],fsize);
        if(ifs.good() == false)
        {
            ELOG("%s read file failed!",filename.c_str());
            ifs.close();
            return false;
        }
        //关闭文件
        ifs.close();
        return true;
    }
};

