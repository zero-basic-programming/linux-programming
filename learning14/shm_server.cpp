#include "comm.hpp"

int main()
{
    //服务端先打开共享内存
    //获取k
    key_t k = getkey(PATH_NAME);
    assert(k != -1);
    //创建共享内存
    int shmid = creatshm(k);
    assert(shmid != -1);
    //进行共享内存的关联
    char* start = (char*)shmattach(shmid);
    //使用共享内存进行进程间通信
    while(1)
    {
        //我们可以打印出来其中的属性
        printf("client say: %s\n",start);
        struct shmid_ds ds;
        shmctl(shmid,IPC_STAT,&ds);
        printf("获得属性： 大小：%d,id:%d ,进程id:%d\n",ds.shm_segsz,ds.shm_cpid,getpid());
        sleep(1);
    }
    //去关联
    detachshm(start);
    //关闭共享内存
    delshm(shmid);
    return 0;
}