#include "comm.hpp"

int main()
{
    key_t k = getkey(PATH_NAME);
    //创建共享内存
    int shmid = getshm(k);
    //关联
    char* start = (char*)shmattach(shmid);
    //通信
    const char* str = "I am client say to server";
    int cnt = 1;
    int id = getpid();
    while(1)
    {
        sleep(1);
        snprintf(start,SIZE,"%s : id:%d 编号:%d",str,id,cnt++);
    }
    //去关联
    detachshm(start);
    return 0;
}