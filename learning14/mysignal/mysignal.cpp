#include <iostream>
#include <signal.h>
#include <unistd.h>
using namespace std;

void helper(int signo)
{
    cout<<"捕捉到一个信号，其信息编号是"<<signo<<endl;
}

int main()
{
    signal(2,helper);
    while(true)
    {
        cout<<"我是一个进程，我的id是："<<getpid()<<endl;
        sleep(1);
    }
    return 0;
}