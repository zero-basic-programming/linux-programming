#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <cassert>
using namespace std;

//全局变量n
//volatile,保证内存可见性
//volatile int n = 0;
void Count(int n)
{
    //用来计数
    while(n)
    {
        printf("count: %2d\r",n);
        fflush(stdout);
        --n;
        sleep(1);
    }
    cout<<endl;
}

// void handler(int signo)
// {
//     cout<<n<<endl;
//     ++n;
//     cout<<n<<endl;
// }
int main()
{
    //显示设置，这样OS可以自动的回收子进程
    signal(SIGCHLD,SIG_IGN);
    cout<<"我是父进程，我的ID是："<<getpid()<<endl;
    int n = fork();
    if(n == 0)
    {
        printf("我是一个子进程，我的id是 %d，父进程id是： %d\n",getpid(),getppid());
        Count(10);
        exit(1);
    }
    //父进程什么都不做，不需要回收子进程，
    while(true)
        sleep(1);
    // signal(2,handler);
    // while(!n)
    // {
    //     //这里什么都不做，如果没有退出就说明有问题
    // }
    // cout<<"我是一个正常退出的进程"<<endl;
    return 0;
}
// void handler(int signo)
// {
//     cout<<"接受到一个信号，signo:"<<signo<<endl;
//     Count(10);
// }
// int main()
// {
//     struct sigaction act,oact;
//     act.sa_flags = 0;
//     act.sa_handler = handler;
//     sigemptyset(&act.sa_mask);
//     //我们也可以把其他信号屏蔽放进来
//     sigaddset(&act.sa_mask,3);
//     sigaction(2,&act,&oact);
//     while(1)
//     {
//         //cout<<"mypid :"<<getpid()<<endl;
//         sleep(1);
//     }
//     return 0;
// }