#include <iostream>
#include <pthread.h>

//使用语言来封装系统的Mutex
class Mutex
{
public:
    Mutex(pthread_mutex_t* p = nullptr)
    :_mutex(p)
    {};
    void lock()
    {
        if(_mutex) pthread_mutex_lock(_mutex);
    }
    void unlock()
    {
        if(_mutex) pthread_mutex_unlock(_mutex);
    }
private:
    pthread_mutex_t* _mutex;
};

//使用RAII方式

class lockGuard
{
public:
    lockGuard(pthread_mutex_t* p)
    :_mp(p)
    {
        //加锁
        _mp.lock();
    }
    ~lockGuard()
    {
        //解锁
        _mp.unlock();
    }
private:
    Mutex _mp;
};
