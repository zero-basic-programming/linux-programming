#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <pwd.h>
using namespace std;


// int main()
// {
//     int i = 0;
//     char buf[101];
//     memset(buf,0,sizeof buf);
//     const char* lable = "-\|/";
//     while(i<=100)
//     {
//         printf("[%-100s][%%%d][%c]\r",buf,i,lable[i%4]);
//         fflush(stdout);
//         buf[i++] = '#';
//         usleep(10000);
//     }
//     std::cout<<std::endl;
//     return 0;
// }


// int main()
// {
//     int ret = fork();
//     if(ret < 0)
//     {
//         cout << "fork failed!" <<endl;
//         return -1;
//     }
//     else if(ret == 0)
//     {
//         //子进程
//         cout << "I am child ..." << endl;
//         exit(0);
//     }
//     //父进程
//     cout << "Main process" <<endl;
//     sleep(1); //保证父子进程都执行完自己的代码
//     return 0;
// }

// int main()
// {
//     int ret = fork();
//     if(ret < 0)
//     {
//         cout << "fork failed!" <<endl;
//         return -1;
//     }
//     else if(ret == 0)
//     {
//         //子进程
//         cout << "I am child , Now I am sleeping" << endl;
//         sleep(5);
//         exit(0);  //子进程先退出
//     }
//     //父进程
//     cout << "Main process" <<endl;
//     sleep(30); 
//     return 0;
// }


// int main()
// {
//     pid_t ret = fork();
//     if(ret < 0)
//     {
//         cout << "fork failed!" <<endl;
//         return -1;
//     }
//     else if(ret == 0)
//     {
//         //子进程
//         cout << "I am child , Now I am sleeping ： pid:" << getpid() << endl;
//         sleep(10);
//         exit(0);  
//     }
//     //父进程
//     cout << "Main process pid:" << getpid() <<endl;
//     sleep(5);  //父进程先退出，子进程会被1号进程领养
//     return 0;
// }


// int main(int argc,char* argv[])
// {
//     if(argc > 1)
//     {
//         if(strcmp(argv[1],"-a") == 0)
//         {
//             cout << "you use -a option" <<endl;
//         }
//         else if(strcmp(argv[1],"-b") == 0)
//         {
//             cout << "you use -b option" <<endl;
//         }
//         else{
//             cout << "you use -c option" <<endl;
//         }
//     }
//     else{
//         cout << "you use any option" <<endl;
//     }
//     return 0;
// }


//打印环境变量表
// int main(int argc,char* argv[],char* env[])
// {
//     for(int i = 0;env[i] != nullptr;++i)
//     {
//         cout << env[i] << endl;
//     }
//     return 0;
// }

//另外一种方式
// int main()
// {
//     extern char** environ;
//     for(int i = 0;environ[i] != nullptr;++i)
//     {
//         cout << environ[i] << endl;
//     }
//     return 0;
// }

// int main()
// {
//     pid_t id = fork();
//     // 默认fork不会失败
//     if(id == 0)
//     {
//         for(int i = 0;i<10;++i)
//         {
//             printf("I am child pid : %d , ppid : %d\n",getpid(),getppid()); 
//             sleep(1);
//         }
//         exit(0);
//     }
//     //父进程
//     int status = 0;
//     int ret = wait(&status);
//     if(ret > 0)
//     {
//         cout << "wait child success" <<endl;
//         //打印退出码
//         if(WIFEXITED(status)) //正常退出
//         {
//             cout << "child exit code : " << WEXITSTATUS(status) << endl;
//         }
//         else{
//             //信号退出
//             cout << "sign exit : " << (status & 0x7F) <<endl;
//         }
//     }
//     sleep(3);
//     return 0;
// }


//多进程，通过父进程创建多个子进程
// int main()
// {
//     pid_t ids[10];
//     for(int i = 0 ;i<10;++i)
//     {
//         pid_t id = fork();
//         if(id == 0)
//         {
//             cout << " I am child process success pid :" << getpid() <<endl;
//             sleep(3);
//             exit(i); //把退出码设计成i方便观察
//         }
//         //father
//         ids[i] = id;
//     }
//     //父进程等待子进程退出
//     for(int i = 0;i<10;++i)
//     {
//         int status = 0;
//         int ret = wait(&status);
//         if(ret > 0)
//         {
//             cout << "wait child success" <<endl;
//             //打印退出码
//             if(WIFEXITED(status)) //正常退出
//             {
//                 cout << "child exit code : " << WEXITSTATUS(status) << endl;
//             }
//             else{
//                 //信号退出
//                 cout << "sign exit : " << (status & 0x7F) <<endl;
//             }
//         }
//     }
//     sleep(3);
//     return 0;
// }


//非阻塞等待子进程
// int main()
// {
//     pid_t id = fork();
//     if(id == 0)
//     {
//         cout << "I am child pid:" << getpid() << endl;
//         sleep(3);
//         exit(0);
//     }
//     //father
//     int status = 0;
//     while(1)
//     {
//         int ret = waitpid(id,&status,WNOHANG);
//         if(ret > 0)
//         {
//             //等待成功
//             cout << "wait child success,exit code:" << WEXITSTATUS(status) << endl;
//             break;
//         }
//         else if(ret == 0)
//         {
//             cout << "father process doing other thing" << endl;
//             sleep(1);
//         }
//         else
//         {
//             //等待失败
//             cout << "wait failed !" <<endl;
//             break;
//         }
//     }
//     return 0;
// }


