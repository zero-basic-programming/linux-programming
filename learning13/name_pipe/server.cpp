#include "comm.hpp"

int main()
{
    //首先创建命名管道
    bool n = creat_pipe(NAME_PIPE);
    assert(n);
    //准备接受信号
    cout<<"server begin";
    int rfd = open(NAME_PIPE,O_RDONLY);
    assert(rfd>0);
    cout<<"server end";

    char buffer[1024];
    while(true)
    {
        //接受信息
        ssize_t s = read(rfd,buffer,sizeof(buffer)-1);
        if(s > 0)
        {
            //接受到信息了
            buffer[s] = 0;//buffer中没有拷贝\0
            cout<<"client -> server"<<buffer<<endl;
        }
        else if(s == 0)
        {
            cout<<"client quit,me too"<<endl;
            break;
        }
        else
        {
            //错误
            cout<<"err string"<<endl;
            exit(-1);
        }
    }
    close(rfd);
    remove_pipe(NAME_PIPE);
    return 0;
}