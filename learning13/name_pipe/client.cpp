#include "comm.hpp"

int main()
{
    //发送信号
    cout<<"client发送信号"<<endl;
    int wfd = open(NAME_PIPE,O_WRONLY);
    assert(wfd>0);

    char buffer[1024];
    while(true)
    {
        //从屏幕中得到信息，打印到buffer中
        cout<<"please say >>";
        fgets(buffer,sizeof(buffer),stdin);
        if(strlen(buffer)>0)
        {
            //把最后一个字符改成\0,屏幕输入默认加的是\n，所以要更新下
            buffer[strlen(buffer)-1] = 0;
        }
        //写入管道中
        ssize_t s = write(wfd,buffer,strlen(buffer));
        assert(s == strlen(buffer));
        (void)s;
    }
    close(wfd);
    return 0;
}