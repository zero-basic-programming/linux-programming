#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <cassert>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
using namespace std;
#define NAME_PIPE "/tmp/mypipe"


bool creat_pipe(const string& path)
{
    umask(0);
    int k = mkfifo(path.c_str(),0600);
    //assert(k == 0);
    if(k == 0)
        return true;
    else 
        std::cout << "errno: " << errno << " err string: " << strerror(errno) << std::endl;
        return false;
}

void remove_pipe(const string& path)
{
    int n = unlink(path.c_str());
    assert(n == 0);
    (void)n;//这里之后n就不再使用了，防止编译报错
}