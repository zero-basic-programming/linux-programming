#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <sys/epoll.h>
#include "err.hpp"
#include "log.hpp"

static const int defaultfd = -1;
static const int size = 64;
class Epoller
{
public:
    Epoller()
    :_epfd(defaultfd)
    {
    }
    ~Epoller()
    {
    }
    void Create()
    {
        _epfd = epoll_create(size);
        if(_epfd == -1)
        {
            logMessage(FATAL,"create epoll fail!");
            exit(EPOLL_CREATE_ERR);
        }
    }
    int Wait(struct epoll_event* revs,int num,int timeout)
    {
        int n = epoll_wait(_epfd,revs,num,timeout);
        return n;
    }
    void Close()
    {
        close(_epfd);
    }
    bool AddEvent(uint32_t event,int sock)
    {
        struct epoll_event ev;
        ev.data.fd = sock;
        ev.events = event;
        int n = epoll_ctl(_epfd,EPOLL_CTL_ADD,sock,&ev);
        return n == 0;
    }
    bool Control(int sock,uint32_t event,int action)
    {
        int n = 0;
        if(action == EPOLL_CTL_MOD)
        {
            struct epoll_event ev;
            ev.data.fd = sock;
            ev.events = event;
            n = epoll_ctl(_epfd,action,sock,&ev);
        }
        else if(action == EPOLL_CTL_DEL)
        {
            n = epoll_ctl(_epfd,action,sock,nullptr);
        }
        else n = -1;
        return n == 0;
    }
private:
    int _epfd;
};