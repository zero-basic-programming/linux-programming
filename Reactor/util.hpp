#pragma once
#include <unistd.h>
#include <fcntl.h>

class util
{
public:
    static bool SetNonBlock(int fd)
    {
        int fl = fcntl(fd, F_GETFL);
        if (fl < 0)
            return false;
        fcntl(fd, F_SETFL, O_NONBLOCK);
        return true;
    }
};