#include <iostream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include "Util.hpp"
using namespace std;
const string default_root = "./wwwroot";
const string home_page = "index.html";  //默认的开始目录
const string html_404 = "/home/liang/linux-programming/learning27/wwwroot/404.html";
class httpRequest
{
public:
    httpRequest()
    {}
    bool prase()
    {
        //1.把inbuffer的内容切分成多分内容
        string line = Util::GetOneline(inbuffer);
        if(line.empty())   return false;
        //2.从第一行中拿到3个字段
        stringstream ss(line);
        ss >> method >> url >> httpversion;
        //3.添加web路径
        path = default_root;
        path += url; //./wwwroot/
        if(path[path.size()-1] == '/')  path += home_page;  //这里默认到我们设定的首页
        //4.得到文件的后缀
        auto pos = path.rfind('.');
        if(pos == string::npos) 
            suffix = ".html"; //这里没有找到默认添加为.html格式
        else    suffix = path.substr(pos);
        //5.得到资源的大小
        struct stat st;
        int n = stat(path.c_str(),&st);
        if(n == 0)  size = st.st_size;
        else size = -1;
        return true;
    }
    ~httpRequest()
    {}
public:
    std::string inbuffer; //这里是读到的数据
    std::string method; //方法
    std::string url;  //网址
    std::string httpversion; // 版本
    std::string suffix; //文件的后缀
    int size; //资源大小
    std::string path; //路径
};


class httpResponse
{
public:
    std::string outbuffer; //这里先做简单的测试
};