#pragma once 
#include <iostream>
#include <string>
#include <fstream>
#include <fcntl.h>
#include <cstdio>
#include <cstring>
using namespace std;

const string sep = "\r\n";

class Util
{
public:
    static string GetOneline(string& in)
    {
        auto pos = in.find(sep);
        if(pos == string::npos) return "";
        string ret = in.substr(0,pos);
        //in.erase(ret.size()+sep.size());
        return ret;
    }
    static bool readFile(const string path,char* buffer,int size)
    {
        ifstream in(path,ifstream::in | ios::binary);
        cout<<"path: "<<path<<endl;
        if(!in.is_open()) 
        {  
            cout<<"___________404file fail"<<endl;
            return false;
        }
        in.read(buffer,size);
        in.close();
        return true;
    }
    // static bool readFile(const string path,char* buffer,int size)
    // {
    //     FILE* pf = fopen(path.c_str(),"rb");
    //     if(pf == nullptr)
    //     {
    //         cout<<"fopen fail: "<<errno<<"strerrno: "<<strerror(errno)<<endl;
    //     }
    //     char ch = fgetc(pf);
    //     while(ch != EOF)
    //     {
    //         sprintf(buffer,"%c",ch);
    //         ch = fgetc(pf);
    //     }
    //     cout<<"open success"<<endl;
    //     fclose(pf);
    // }
    // static bool readFile1(const std::string resource, std::string *out)
    // {
    //     std::ifstream in(resource);
    //     if(!in.is_open()) return false; // resource not found

    //     std::string line;
    //     while(std::getline(in, line))
    //     {
    //         *out += line;
    //     }

    //     in.close();
    //     return true;
    // }
};