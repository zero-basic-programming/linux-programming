#include "../source/server.hpp"


void OnConnect(const PtrConnection& conn)
{
    DLOG("ONConnect %p",conn.get());
}

void OnClose(const PtrConnection& conn)
{
    DLOG("CloseConnect %p",conn.get());
}

void MsgConnect(const PtrConnection& conn,Buffer* buffer)
{
    //接收信息，然后转发
    DLOG("%s",buffer->ReadPosition());
    buffer->MoveReadOffSet(buffer->ReadAbleSize());
    std::string str = "echo hello!";
    conn->Send(str.c_str(),str.size());
}

int main()
{
    TcpServer tcpsvr(8083);
    tcpsvr.SetThreadCount(2);
    tcpsvr.SetConnectCallback(OnConnect);
    tcpsvr.SetClosedCallback(OnClose);
    tcpsvr.SetMessageCallback(MsgConnect);
    tcpsvr.SetActiveRelease(10);
    tcpsvr.Start();
    return 0;
}