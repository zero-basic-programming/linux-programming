//一次性向服务器发送多个请求，查看服务器的响应情况
//每一条请求都应该得到正确的处理
#include "../source/server.hpp"

int main()
{
    Socket cli_sock;
    cli_sock.CreateClient(8080, "127.0.0.1");
    std::string req = "GET /hello HTTP/1.1\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n";
    req += "GET /hello HTTP/1.1\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n";
    req += "GET /hello HTTP/1.1\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n";
    while(1)
    {
        assert(cli_sock.Send(req.c_str(),req.size()) != -1);
        char buffer[1024];
        assert(cli_sock.Recv(buffer,1023) != -1);
        DLOG("[%s]",buffer);
        sleep(3);
    }
    cli_sock.Close();
    return 0;
}