//测试在服务器达到瓶颈的时候对连接的处理，其他的连接可能因为这个连接处理而导致超时销毁

#include "../source/server.hpp"


int main()
{
    signal(SIGCHLD,SIG_IGN);
    for(int i = 0;i<10;++i)
    {
        pid_t pid = fork();
        if(pid < 0)
        {
            DLOG("FORK ERROR");
            return -1;
        }
        else if(pid == 0)
        {
            //子进程
            Socket cli_sock;
            cli_sock.CreateClient(8080, "127.0.0.1");
            std::string req = "GET /hello HTTP/1.1\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n";
            while(1)
            {
                assert(cli_sock.Send(req.c_str(),req.size()) != -1);
                char buffer[1024];
                assert(cli_sock.Recv(buffer,1023) != -1);
                DLOG("[%s]",buffer);
                sleep(3);
            }
            cli_sock.Close();
            exit(0);
        }
    }
    while(1) sleep(1);
    return 0;
}