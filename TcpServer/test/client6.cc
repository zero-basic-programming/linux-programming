//测试大文件上传

#include "../source/http/http.hpp"

int main()
{
    Socket cli_sock;
    cli_sock.CreateClient(8080, "127.0.0.1");
    std::string req = "PUT /submit.txt HTTP/1.1\r\nConnection: keep-alive\r\n";
    std::string body;
    bool ret = Util::ReadFile("./hello.txt",&body);
    req += "Content-Length: " + std::to_string(body.size()) + "\r\n\r\n";
    assert(cli_sock.Send(req.c_str(),req.size()) != -1);
    //传输正文
    assert(cli_sock.Send(body.c_str(),body.size()) != -1);
    char buffer[1024];
    assert(cli_sock.Recv(buffer,1023) != -1);
    DLOG("[%s]",buffer);
    sleep(3);
    cli_sock.Close();
    return 0;
}