#include "../source/server.hpp"

int main()
{
    Socket cli_sock;
    cli_sock.CreateClient(8083, "127.0.0.1");
    DLOG("client fd:%d",cli_sock.Fd());
    for(int i = 0;i<5;++i) //进行5次通信
    {
        std::string str = "hello!";
        cli_sock.Send(str.c_str(), str.size());
        char buffer[1024] = {0};
        cli_sock.Recv(buffer, 1023);
        DLOG("server echo:%s", buffer);
        sleep(1);
    }
    while(1) sleep(1); 
    return 0;
}