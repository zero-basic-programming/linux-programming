//只发送一次小的请求，服务器得不到完整的数据，就不会进行业务处理
//给服务器发送多条小的请求，服务器会把后面的请求当正文处理，但是后面的请求会失败
#include "../source/server.hpp"

int main()
{
    Socket cli_sock;
    cli_sock.CreateClient(8080, "127.0.0.1");
    std::string req = "GET /hello HTTP/1.1\r\nConnection: keep-alive\r\nContent-Length: 100\r\n\r\nhow are you?";
    while(1)
    {
        assert(cli_sock.Send(req.c_str(),req.size()) != -1);
        assert(cli_sock.Send(req.c_str(),req.size()) != -1);
        assert(cli_sock.Send(req.c_str(),req.size()) != -1);
        char buffer[1024];
        assert(cli_sock.Recv(buffer,1023) != -1);
        DLOG("[%s]",buffer);
        sleep(3);
    }
    cli_sock.Close();
    return 0;
}