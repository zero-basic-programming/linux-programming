#include <iostream>
#include <string>
#include <regex>


int main()
{
    // 请求： GET /hello/login?user=xiaoming&passwd=123456 HTTP/1.1\r\n
    std::string str = "get /hello/login?user=xiaoming&passwd=123456 HTTP/1.1";
    //提取请求方法
    std::smatch matches;
    std::regex e("(GET|POST|HEAD|DELETE|PUT) ([^?]*)(?:\\?(.*))? (HTTP/1\\.[01])(?:\r|\r\n)?",std::regex::icase); //匹配请求方法的正则表达式
    //(GET|POST|HEAD|DELETE|PUT) 匹配其中任意一个请求方法
    //([^?]*) 匹配非？字符 *表示0次或者多次
    //\\?(.*) 提取?问号之后的字符，直到遇到后面的空格
    //[01] 提起0或者1其中任意一个
    //(?:\r|\r\n)?  ?:表示匹配某个字符串，但是不提取  后面的？表示匹配0次或者1次
    bool ret = regex_match(str,matches,e);
    if(ret == false) return -1;
    std::string method = matches[1];
    std::transform(method.begin(),method.end(),method.begin(),::toupper);  //转换大小写
    std::cout<<method<<std::endl;
    for(int i = 0;i<matches.size();++i)
    {
        std::cout<< i <<" : ";
        std::cout<<matches[i]<<std::endl;
    }
    return 0;
}