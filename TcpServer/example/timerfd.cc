#include <iostream>
#include <unistd.h>
#include <sys/timerfd.h>

int main()
{
    //int timerfd_create(int clockid, int flags);
    int timerfd = timerfd_create(CLOCK_MONOTONIC,0);
    if(timerfd < 0)
    {
        perror("timerfd_create fail");
        return -1;
    }
    //int timerfd_settime(int fd, int flags,const struct itimerspec *new_value,struct itimerspec *old_value);
    //设置结构体
    struct itimerspec itim;
    itim.it_value.tv_sec = 1;
    itim.it_value.tv_nsec = 0; //设置第一次超时时间
    itim.it_interval.tv_sec = 1;
    itim.it_interval.tv_nsec = 0; //第一次超时之后每隔1秒超时一次
    int ret = timerfd_settime(timerfd,0,&itim,nullptr);
    if(ret != 0)
    {
        perror("timerfd_settime fail");
        return -1;
    }
    while(1){
        //因为这个也是一个文件描述符，所以可以使用read进行系统调用，来读取到其中的数据
        uint64_t times = 0;
        int ret = read(timerfd,&times,8);
        if(ret < 0){
            perror("read fail");
            return -1;
        }
        std::cout<<times<<std::endl;
    }
    return 0;
}