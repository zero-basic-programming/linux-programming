#include <iostream>
#include <string>
#include <regex>


int main()
{
    std::string str = "/numbers/1234";
    std::smatch matches; //存放结果的容器
    std::regex e("/numbers/(\\d+)");  //第一个\用来转义
    
    bool ret = std::regex_match(str,matches,e);
    if(ret == false) return -1;
    //得到结果
    for(auto& s:matches)
    {
        std::cout<<s<<std::endl;
    }
    return 0;
}