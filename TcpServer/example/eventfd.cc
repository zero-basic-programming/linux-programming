#include <iostream>
#include <unistd.h>
#include <sys/eventfd.h>

int main()
{
    int efd = eventfd(0,EFD_CLOEXEC | EFD_NONBLOCK);
    if(efd < 0)
    {
        perror("eventfd failed!");
        return -1;
    }
    //向文件写入
    uint64_t val = 1;
    write(efd,&val,sizeof(val));
    write(efd,&val,sizeof(val));
    write(efd,&val,sizeof(val));
    //读取文件
    uint64_t res;
    read(efd,&res,sizeof(res));
    std::cout<<res<<std::endl;
    return 0;
}