#include "../server.hpp"

class EchoServer
{
private:   
    TcpServer _server;
private:    
    void OnConnect(const PtrConnection& conn)
    {
        DLOG("ONConnect %p",conn.get());
    }

    void OnClose(const PtrConnection& conn)
    {
        DLOG("CloseConnect %p",conn.get());
    }

    void MsgConnect(const PtrConnection& conn,Buffer* buffer)
    {
        conn->Send(buffer->ReadPosition(),buffer->ReadAbleSize());
        buffer->MoveReadOffSet(buffer->ReadAbleSize());
        conn->ShutDown();
    }
public: 
    EchoServer(int port):_server(port)
    {
        _server.SetThreadCount(2);
        _server.SetConnectCallback(std::bind(&EchoServer::OnConnect,this,std::placeholders::_1));
        _server.SetClosedCallback(std::bind(&EchoServer::OnClose,this,std::placeholders::_1));
        _server.SetMessageCallback(std::bind(&EchoServer::MsgConnect,this,std::placeholders::_1,std::placeholders::_2));
        _server.SetActiveRelease(10);
        _server.Start();
    }
    void Start() { _server.Start(); }
};