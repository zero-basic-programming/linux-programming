#include <iostream>
#include "udpServer.hpp"
#include <memory>
#include <unordered_map>
#include <fstream>
#include <string>
#include <signal.h>
using namespace std;
using namespace myserver;

unordered_map<string, string> mp;

static void Usage(string str)
{
    cout << "\nUsage\n\t" << str << "local port" << endl;
}


void handlerMessage(int fd, string ip, uint16_t port, string message)
{
    string response_message;
    message += "server handler ";
    // 开始返回
    struct sockaddr_in tmp;
    bzero(&tmp, sizeof(tmp));
    tmp.sin_family = AF_INET;
    tmp.sin_addr.s_addr = inet_addr(ip.c_str());
    tmp.sin_port = htons(port);
    sendto(fd, response_message.c_str(), sizeof(response_message), 0, (struct sockaddr *)&tmp, sizeof(tmp));
}


int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }
    uint16_t port = atoi(argv[1]);
    unique_ptr<udpServer> user(new udpServer(handlerMessage ,port));
   
    user->initServer();
    user->start();
    return 0;
}