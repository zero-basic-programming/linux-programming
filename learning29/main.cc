#include "err.hpp"
#include "poll_server.hpp"
#include <memory>

void Usage(char* argv)
{
    cout<<"Usage:\n\t"<<argv<<"port"<<endl<<endl;
}

string transaction(const string& request)
{
    //这里就是之间返回，测试成功即可
    return request;
}

int main(int argc,char* argv[])
{
    if(argc != 2)
    {
        //使用手册
        Usage(argv[0]);
         exit(USAGE_ERR);
    }
    unique_ptr<pollServer> usvr(new pollServer(transaction));
    usvr->initServer();
    usvr->start();
    return 0;
}