#pragma once
#include <string>
#include <functional>
#define NUM 1024
using namespace std;


void serviceIO(int sock)
    {
        // 这里都是文件操作
        // 读到客户端发来的信息
        char buffer[NUM];
        while (true)
        {
            ssize_t n = read(sock, buffer, sizeof(buffer) - 1);
            if(n > 0)
            {
                buffer[n] = 0;
                //解析信息
                cout<<"服务器收到消息："<<buffer<<endl;
                //回复client
                string response_message = buffer;
                response_message += " server[echo] ";
                //通过文件的写操作写到sock中
                write(sock,response_message.c_str(),response_message.size());
            }
            else if(n == 0)
            {
                //这里客户端已经退出，服务器该sock也应该结束
                cout<<"客户端退出，me too!"<<endl;
                break; //这里后面应该采用多进程或者多线程的方式去处理这个问题，这里只有当一个客户端退出时才能给另外一个客户端进行通信
            }
        }
    }


// 写一个仿函数用来让消费线程完成特定的任务
class Task
{
    using func_t = std::function<void(int)>;

public:
    Task()
    {
    }
    Task(int sock ,func_t func)
        :_sock(sock), _callback(func)
    {
    }
    string operator()()
    {
        _callback(_sock);
    }
private:
    int _sock;
    func_t _callback;
};

