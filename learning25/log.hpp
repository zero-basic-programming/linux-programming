#pragma once
#include <iostream>
#include <string>
#include <time.h>
#include <unistd.h>
#include <cstdarg>
using namespace std;

#define LOG_NORMAL "log.txt"
#define LOG_ERROR "log.error"

#define DEBUG 0
#define NORMAL 1
#define WARNING 2
#define ERROR 3
#define FATAL 4 // 致命的
#define NUM 1024

const char *to_levelstr(int level)
{
    switch (level)
    {
    case DEBUG:
        return "DEBUG";
    case NORMAL:
        return "NORMAL";
    case WARNING:
        return "WARNING";
    case ERROR:
        return "ERROR";
    case FATAL:
        return "FATAL";
    default:
        return nullptr;
    }
}

static void logmessage(int level, const char *format, ...)
{
    //[日志等级] [时间戳/时间] [pid] [messge]
    // 像打印一样，可以打印到文件中
    char prelog[NUM]; // 这里主要打印前3个
    snprintf(prelog, sizeof(prelog), "[%s][%ld][%d]", to_levelstr(level), (long int)time(nullptr), getpid());
    char logcontext[NUM]; // 打印信息
    va_list arg;
    va_start(arg, format);
    vsnprintf(logcontext, sizeof(logcontext), format, arg);
    // 打开文件，向文件写入
    FILE *log = fopen(LOG_NORMAL, "a");
    FILE *err = fopen(LOG_ERROR, "a");
    if (log && err)
    {
        FILE *cur = nullptr;
        if (level == DEBUG || level == NORMAL || level == WARNING)
            cur = log;
        if (level == ERROR || level == FATAL)
            cur = err;
        if(cur)  fprintf(cur,"%s%s\n",prelog,logcontext);
        fclose(log);
        fclose(err);
    }
}

//  static void logmessage(int level, const char *format)
//  {
//     cout<<format<<endl;
//  }
