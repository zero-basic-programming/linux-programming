#include "tcpServer.hpp"
#include "daemon.hpp"
#include <memory>

//tcpServer  serverport
static void Usage(char* s)
{
    cout<<"\nUsage:\n\t"<<s<<" serverport"<<endl;
}

int main(int argc,char* argv[])
{
    if(argc != 2)
    {
        //使用手册
        Usage(argv[0]);
        exit(USAGE_ERROR);
    }
    uint16_t port = atoi(argv[1]);
    unique_ptr<tcpserver> ts(new tcpserver(port));
    ts->initServer();
    //为了能保证服务器在后台也能运行，将其变成守护进程
    daemonSelf();
    ts->start();
    return 0;
}