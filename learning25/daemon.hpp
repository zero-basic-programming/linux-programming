#pragma once
#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <cassert>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define DEV "/dev/null" // 这个文件就是用来放没用的信息的

static void daemonSelf(const char *path = nullptr)
{
    // 1.屏蔽异常的信号
    signal(SIGPIPE, SIG_IGN);

    // 2.让当前进程不是组长
    if (fork() > 0)
    {
        // 父进程退出，这样就可以保证当前进程不是组长了
        exit(0);
    }

    // 3.调用系统接口，变成守护进程
    int n = setsid();
    assert(n != -1);

    // 4.把0，1，2的文件描述符重定向到文件中，或者关闭
    int fd = open(DEV,O_RDWR); 
    if(fd >= 0)
    {
        dup2(fd,0);
        dup2(fd,1);
        dup2(fd,2);
    }
    else
    {
        //打开失败，这里直接关闭
        close(0);
        close(1);
        close(2);
    }

    // 5.更改默认的路径
    if(path) chdir(path);
}