#pragma once 
#include <pthread.h>

class mutex
{
public:
    mutex(pthread_mutex_t* mutex = nullptr)
    :_mutex(mutex)
    {
        pthread_mutex_init(_mutex,nullptr);
    }
    void lock()
    {
        if(_mutex)
            pthread_mutex_lock(_mutex);
    }
    void unlock()
    {
        if(_mutex)
            pthread_mutex_unlock(_mutex);
    }
    ~mutex()
    {
        pthread_mutex_destroy(_mutex);
    }
private:
    pthread_mutex_t* _mutex;
};

//使用RAII

class LockGuard
{
public:
    LockGuard(pthread_mutex_t* mtx)
    :_mtx(mtx)
    {
        _mtx.lock();
    }
    ~LockGuard()
    {
        _mtx.unlock();
    }
private:
    mutex _mtx;
};