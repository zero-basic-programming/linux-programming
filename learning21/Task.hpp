#pragma once
#include <string>
#include <functional>
// 写一个仿函数用来让消费线程完成特定的任务
class Task
{
    using func_t = std::function<int(int, int, char)>;

public:
    Task()
    {
    }
    Task(int x, int y, func_t func, const char op)
        : _x(x), _y(y), _op(op), _callback(func)
    {
    }
    string operator()()
    {
        int result = _callback(_x, _y, _op);
        char buffer[1024];
        snprintf(buffer, sizeof buffer, "%d %c %d = %d", _x, _op, _y, result);
        return buffer;
    }
    string ToString()
    {
        char buffer[1024];
        snprintf(buffer, sizeof buffer, "%d %c %d = ?", _x, _op, _y);
        return buffer;
    }

private:
    int _x;
    int _y;
    char _op; // 操作方式
    func_t _callback;
};

// 我们想要的运算方式
const string str = "+-*/%";

int mymath(int x, int y, int op)
{
    int result = 0;
    switch (op)
    {
    case '+':
        result = x + y;
        break;
    case '-':
        result = x - y;
        break;
    case '*':
        result = x * y;
        break;
    case '/':
        if (y == 0)
        {
            cout << "/0错误" << endl;
            result = -1; // 假设错误返回-1
            break;
        }
        result = x / y;
        break;
    case '%':
        if (y == 0)
        {
            cout << "%0错误" << endl;
            result = -1; // 假设错误返回-1
            break;
        }
        result = x % y;
        break;
    }
    return result;
}
