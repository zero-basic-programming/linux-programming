#include "calClient.hpp"
#include <memory>
using namespace std;

//tcpClient  serverip serverport
static void Usage(char* s)
{
    cout<<"\nUsage:\n\t"<<s<<" serverip serverport"<<endl;
}

int main(int argc,char* argv[])
{
    if(argc != 3)
    {
        //使用手册
        Usage(argv[0]);
        exit(1);
    }
    string ip = argv[1];
    uint16_t port = atoi(argv[2]);
    unique_ptr<calclient> tcli(new calclient(ip,port));
    tcli->initclient();
    tcli->start();
    return 0;
}