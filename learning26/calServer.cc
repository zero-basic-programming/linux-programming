#include "calServer.hpp"
#include <memory>

// tcpServer  serverport
static void Usage(char *s)
{
    cout << "\nUsage:\n\t" << s << " serverport" << endl;
}

// 这里进行业务逻辑处理，使用回调的方式处理
bool cal(const Request &req, Response &resp)
{
    resp._exitcode = OK,
    resp._result = OK;
    switch (req._op)
    {
    case '+':
        resp._result = req._x + req._y;
        break;
    case '-':
        resp._result = req._x - req._y;
        break;
    case '*':
        resp._result = req._x * req._y;
        break;
    case '/':
        if (req._y != 0)
        {
            resp._result = req._x / req._y;
        }
        else
        {
            resp._exitcode = DIV_ERROR; // 这里直接传出错误码即可
        }
        break;
    case '%':
        if (req._y != 0)
        {
            resp._result = req._x % req._y;
        }
        else
        {
            resp._exitcode = MOD_ERROR; // 这里直接传出错误码即可
        }
        break;
    default:
        resp._exitcode = OP_ERROR;
        break;
    }
    cout<<"计算完成，结果是："<<resp._result<<endl;
    return true;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        // 使用手册
        Usage(argv[0]);
        exit(USAGE_ERROR);
    }
    uint16_t port = atoi(argv[1]);
    unique_ptr<calserver> ts(new calserver(port));
    ts->initServer();
    ts->start(cal);
    return 0;
}