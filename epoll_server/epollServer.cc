#include "epollServer.hpp"
#include <memory>

using namespace std;

static void usage(string proc)
{
    cerr << "Usage:\n\t" << proc << " port" << "\n\n";
}

string echo(const string &message)
{
    return "I am epollserver, " + message; 
}

// ./epoll_server port
int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        usage(argv[0]);
        exit(USAGE_ERR);
    }
    uint16_t port = atoi(argv[1]);

    unique_ptr<EpollServer> epollsvr(new EpollServer(echo, port));
    epollsvr->initServer();
    epollsvr->start();

    return 0;
}