#include <iostream>
#include <vector>
#include <unistd.h>
#include <signal.h>
using namespace std;
#define MAX_SIG 31 // 信号的个数
// 创建一个全局的数组，这里可以屏蔽多个信号
static vector<int> sig_arr = {2};

void printsig(const sigset_t &set)
{
    for (int i = MAX_SIG; i > 0; --i)
    {
        // 判断第几号信号是否在pending位图中
        if (sigismember(&set, i))
        {
            cout << "1";
        }
        else cout << "0";
    }
    cout << endl;
}

void myhander(int signo)
{
    cout << signo << "号信号已经执行" << endl;
}

int main()
{
    // 我们可以把信号执行的方法改一下，方便更好的观察
    for (const auto &sig : sig_arr)
    {
        signal(sig, myhander);
    }
    // 设置屏蔽的指定信号
    sigset_t block, oblock, pending;
    // 初始化
    sigemptyset(&block);
    sigemptyset(&oblock);
    sigemptyset(&pending);
    // 添加屏蔽的信号
    for (const auto &sig : sig_arr)
    {
        sigaddset(&block, sig);
    }
    // 把屏蔽的信号设置进内核
    sigprocmask(SIG_SETMASK, &block, &oblock);
    // 然后通过观察pending位图中的01序列即可知道是否完成屏蔽
    // 这里10s后就取消屏蔽
    int cnt = 10;
    while (true)
    {
        // 初始化，这里其实没有必要
        sigemptyset(&pending);
        // 获取
        sigpending(&pending);
        // 自己写一个打印函数去观察
        printsig(pending);
        //1s打印一次
        sleep(1);
        if (cnt-- == 0)
        {
            // 取消屏蔽然后再观察,因为前面已经保存了之前的状态，这里反过来重新设置即可
            sigprocmask(SIG_SETMASK, &oblock, &block);
            // 这里屏蔽信号之后会立刻处理至少一个的信号
            cout << "取消屏蔽信号" << endl;
        }
    }
    return 0;
}