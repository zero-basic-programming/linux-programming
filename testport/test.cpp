#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
using namespace std;
int main()
{
    int listen_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (listen_sock < 0)
    {
        perror(" listen_sock ");
        return 0;
    }
    struct sockaddr_in local;
    bzero(&local, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_port = htons(3389);
    local.sin_addr.s_addr = inet_addr("0.0.0.0");
    int n = bind(listen_sock, (struct sockaddr *)&local, sizeof(local));
    if (n < 0)
    {
        perror(" bind ");
        return 0;
    }
   
    listen(listen_sock,1);

    // 接受新连接
    while (1)
    {
        struct sockaddr_in peer;
        socklen_t peer_len = sizeof(peer);
        int new_sock = accept(listen_sock, (struct sockaddr *)&peer, &peer_len);
        if (new_sock < 0)
        {
            perror(" accept ");
            return 0;
        }
        printf("accept %s:%d,create new_sock %d\n", inet_ntoa(peer.sin_addr), ntohs(peer.sin_port), new_sock);
        char buffer[1024];
        ssize_t s = recv(new_sock,buffer,sizeof(buffer)-1,0);
        if(s < 0)
        {
            perror(" recv ");
            return 0;
        }
    }
    return 0;
}
