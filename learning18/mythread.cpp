#include <iostream>
#include <pthread.h>
#include <cassert>
#include <unistd.h>
#include <cstdio>
#include <vector>
#include <string>
using namespace std;

// void* routine(void* args)
// {
//     const char* s = (const char*)args;
//     while(true)
//     {
//         cout<<"name:"<<s<<endl;
//         sleep(1);
//     }
//     return nullptr;//这样可以让线程退出，当然这里不需要
// }

// int main()
// {
//     //创建一个线程，让这个线程做其他的事情
//     pthread_t tid;
//     int n = pthread_create(&tid,nullptr,routine,(void*)"thread one");
//     assert(0 == n);
//     (void)n;//这里n不再被使用，这样防止出错
//     while(true)
//     {
//         //主线程打印
//         cout<<"我是主线程，正在运行"<<endl;
//         sleep(1);
//     }
//     return 0;
// }

// struct pthread_data
// {
//     int number;
//     pthread_t tid;
//     char numbuffer[64];
// };

// void* pthread_routine(void* args)
// {
//     sleep(1);//等待主进程的打印信息
//     pthread_data* td = static_cast<pthread_data*>(args);
//     int cnt = 10;
//     while(cnt)
//     {
//         cout<<"pthread :"<<td->number<<"pthread name :"<<td->numbuffer<<" cnt :"<<cnt--<<endl;
//         sleep(1);
//     }
//     //return nullptr
//     return (void*)100; //这里回收线程的时候会看到结果
//     //pthread_exit(nullptr); 效果和return nullptr的效果一样
// }

// #define NUM 10
// int main()
// {
//     vector<pthread_data*> vdata;
//     //创建新线程
//     for(int i = 0;i<NUM;++i)
//     {
//         pthread_data* data = new pthread_data();
//         data->number = i+1;
//         snprintf(data->numbuffer,sizeof(data->numbuffer),"%s : %d\n","pthread :",i+1);
//         int n = pthread_create(&data->tid,nullptr,pthread_routine,(void*)data);
//         assert(0 == n);
//         //保存在vector中
//         vdata.push_back(data);
//     }
//     //看看是否成功
//     for(auto it:vdata)
//     {
//         cout<<"pthread :"<<it->number<<"create success"<<endl;
//     }
//     sleep(5);
//     //线程是可以取消的，这里展示一下5s之后取消1到5号线程
//     for(int i = 0;i<vdata.size()/2;++i)
//     {
//         pthread_cancel(vdata[i]->tid);
//         cout<<"cancel pthread number: "<<vdata[i]->number<<endl;
//     }
//     sleep(5);
//     //回收线程
//     for(auto& it:vdata)
//     {
//         void* ret;
//         pthread_join(it->tid,(void**)&ret); 
//         //因为Linux下的指针的大小是8字节的，所以这里要强制成long long才匹配大小
//         cout<<"pthread number:"<<it->number<<"wait success"<<"  return code :"<<(long long)ret<<endl;   
//         delete it;
//     }
//     // while(true)
//     // {
//     //     cout<<"我是主线程，我在等待回收线程"<<endl;
//     //     sleep(1);
//     // }
//     return 0;
// }

int main()
{
    //验证Linux下指针的大小
    void* p = nullptr;
    cout<<sizeof(p)<<endl;
    return 0;
}